FROM nginx:1.17.1-alpine
COPY --from=build /app/dist/angular-keycloak-app /usr/share/nginx/html
EXPOSE 80
